/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
  Є два способи. Перший це створити спочатку елемент за допомогою document.createElement("tag"), а потім додати його на сторінку за допомогою
  Element.prepend()/append/before/after/replaceWith.
  Інший спосіб використати insertAdjacentHTML(), який має два параметри - позиція (куди вставити тег) та текст (запис тегу рядком, як в HTML).

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
  Перший параметр це позиція, на яку необхідно вставити тег. Записується у вигляді рядку. Існує 4 можливі варіанти. 
  beforebegin - вставити тег перед елементом, за його межами.
  afterbegin - вставити тег в середині елементу на початку, новий тег стане першим потомком.
  beforeend - вставити тег в середині елементу в кінці, новий тег стане останнім потомком.
  afterend - вставити тег після елементу, за його межами.

3. Як можна видалити елемент зі сторінки?
  Можна використати .remove() на необхідному елементі.
  Якщо потрібно очистити який елемент від всіх дочірніх елементів та тексту, але залишити сам елемент, то можна використати innerHTML = ""
*/

"use strict";
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.

// Простий варіант, коли відсутні вкладені масиви
  // function getFromArrayToList(arr, parent = document.body) {
  //   let listCollection = arr.map(text=> `<li>${text}</li>`);
  //   let tagListCollection = `<ul>${listCollection.join(" ")}</ul>`;  
  //   return parent.insertAdjacentHTML("afterbegin", tagListCollection);  
  // } 

  function insertListFromArr(arr, parent = document.body) {
    
    function getFromArrToList(arr) {
        let listCollection = arr.map(function (text) {
          if (!Array.isArray(text)) {
          return `<li>${text}</li>`;
          } else {
          return getFromArrToList(text);
          }
        });
        return `<ul>${listCollection.join(" ")}</ul>`;  
      } 
      let textTagInsert = getFromArrToList(arr);
  return parent.insertAdjacentHTML("afterbegin", textTagInsert);    
  }


const testArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const testArray2 = ["1", "2", "3", "sea", "user", 23];
const testArray3  = ["Kharkiv", "Kiev", ["Borispol", ["Airport", "Railway"], "Irpin"], "Odessa", "Lviv", "Dnieper"];
insertListFromArr(testArray3);

// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.
function clearHTML() {
  document.body.innerHTML = "";
}
let interval = 3000;

let endTime = Date.now() + interval;

let timer = setInterval(function() {
  let remainder = endTime - Date.now();
  let seconds = Math.floor(remainder / 1000);
  document.querySelector("#timer").innerHTML = `0${seconds}s to clear`;

  if (remainder <= 0) {
    clearInterval(timer);
  }
}, 500) 

setTimeout(clearHTML, interval);
